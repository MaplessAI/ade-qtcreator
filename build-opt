#!/usr/bin/env bash
#
# Copyright 2020 Mapless AI, Inc.
# Based on ade packages published by Apex.AI, Inc.
# SPDX-License-Identifier: Apache-2.0

set -xe

cd "$(dirname "$(realpath "$0")")"

QTC_VERSION="$1"; shift
# Parse Semantic Versioning MAJOR.MINOR.PATCH string
QTC_MAJOR_MINOR="${QTC_VERSION%.*}"
QTC_PATCH="${QTC_VERSION##*.}"

# Download and install qt-creator
curl -LO "https://download.qt.io/official_releases/qtcreator/${QTC_MAJOR_MINOR}/${QTC_MAJOR_MINOR}.${QTC_PATCH}/qt-creator-opensource-linux-x86_64-${QTC_MAJOR_MINOR}.${QTC_PATCH}.run"
chmod a+x qt-creator-opensource-linux-x86_64-${QTC_MAJOR_MINOR}.${QTC_PATCH}.run
# Non-sudo installation throws exception
export http_proxy=INVALID
./qt-creator-opensource-linux-x86_64-${QTC_MAJOR_MINOR}.${QTC_PATCH}.run --script qt-noninteractive.qs --proxy --platform minimal
mv /opt/qtcreator-${QTC_MAJOR_MINOR}.${QTC_PATCH} _opt_qtcreator
#sudo chown root:root _opt_qtcreator -R
rm qt-creator-opensource-linux-x86_64-${QTC_MAJOR_MINOR}.${QTC_PATCH}.run

# Environment Setup
cp env.sh _opt_qtcreator/.env.sh

# Use _opt_qtcreator for readability, rename to _opt b/c it's expected by Dockerfile
mv _opt_qtcreator _opt
